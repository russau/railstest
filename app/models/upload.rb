class Upload
  # http://blog.isnorcreative.com/2014/05/08/easier-non-activerecord-models-in-rails-4.html
  # http://blog.remarkablelabs.com/2012/12/activemodel-model-rails-4-countdown-to-2013
  include ActiveModel::Model

  attr_accessor :form_action
  attr_accessor :key
  attr_accessor :prefix
  attr_accessor :acl
  attr_accessor :policy_text
  attr_accessor :success_action_redirect

end
