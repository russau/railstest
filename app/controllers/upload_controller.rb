class UploadController < ApplicationController
  def index
    @upload = Upload.new
    @upload.form_action = "https://s3.amazonaws.com/sayersr-demos-uploads/"
    @upload.key = "uploads/#{session[:uid]}/${filename}"
    @upload.prefix = "uploads/#{session[:uid]}/"
    @upload.acl = "private"
    @upload.success_action_redirect = upload_index_url
    @upload.policy_text = '{"expiration": "2016-01-01T00:00:00Z", "conditions":
    [ {"bucket": "sayersr-demos-uploads"}, ["starts-with", "$key", "uploads/'+session[:uid]+'/"], {"acl": "private"},
    {"success_action_redirect": "' + upload_index_url + '"},
    ["starts-with", "$x-amz-security-token", ""], ["content-length-range", 0, 1048000] ] }'

  end
end
