class SessionsController < ApplicationController
  def create
    # provider: amazon
    # uid: amzn1.account.AGR73NTZDPH5ZPIG4IUIISP2R4WQ
    # info: !ruby/hash:OmniAuth::AuthHash::InfoHash
    #   email: russell.sayers@gmail.com
    #   name: Russell Sayers
    # credentials: !ruby/hash:OmniAuth::AuthHash
    #   token: xxxx
    #   refresh_token: xxxx
    #   expires_at: 1424576601
    #   expires: true
    # extra: !ruby/hash:OmniAuth::AuthHash
    #   postal_code:

    # users cannot tamper with session cookie, safe to store auth info in here
    # (so long as secrets.yml is secret!)
    # http://guides.rubyonrails.org/security.html


    auth = request.env['omniauth.auth']
    session[:user_id] = auth.info.email
    session[:token] = auth.credentials.token
    session[:uid] = auth.uid

    logger.info("#{auth.to_yaml}")
    current_user = User.find_by_uid(auth.uid);
    if (current_user.nil?)
      logger.info("user not found #{auth.uid}, creating...")
      user = User.create(uid:auth.uid, email:auth.info.email, name:auth.info.name)
    end

    flash[:notice] = "Welcome back"
    redirect_to '/'
  end

  def logout
    reset_session
    redirect_to '/'
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
